import React, { Component } from 'react'

export class RightSide extends Component {
    render() {
        return (
            <div>
                <div className='flex justify-between ml-72 mt-5 items-center w-36'>
                    <div><img className="h-7 w-7 mb-9" src="Images/notification.png" /></div>
                    <div><img className="h-7 w-7  mb-9" src="Images/comment.png" /></div>
                    <div><img className="h-10 w-10 rounded-full mb-9" src="Images/sa.jpg" /></div>
                </div>
                <button type="button" class="ml-72 bg-yellow-100  rounded-lg text-sm px-7 py-2.5 text-center text-black font-bold">My amazing trip</button>
                <div>
                    <p className='text-4xl ml-5 mt-10 text-gray-200'>I like saying down on<br />
                        the sand and looking at <br />
                        the moon
                    </p>
                    <p className='text-2xl ml-5 mt-20 text-gray-200'>
                        27 people going to this trip
                    </p>
                </div>
                <div className='mt-5 w-96  ml-5 flex justify-between'>
                    <div><img className="mb-9 h-14 w-14 rounded-full" src="Images/sa.jpg" /></div>
                    <div><img className='mb-9 h-14 w-14 rounded-full border-4 border-sky-500' src="Images/raamin.jpg" /></div>
                    <div><img className='mb-9 h-14 w-14 rounded-full border-4 border-red-600' src="Images/nonamesontheway.jpg" /></div>
                    <div><img className='mb-9 h-14 w-14 rounded-full border-4 border-sky-500' src="Images/christina.jpg" /></div>
                    <div className='h-14 w-14 rounded-full bg-orange-50 text-center text-amber-500 border-dashed border-2 border-orange-400'><p className='text-orange-400 font-bold mt-3'>23+</p></div>
                </div>

            </div>
        )
    }
}

export default RightSide