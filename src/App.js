import { useEffect, useState } from 'react';
import './App.css';
import Content from './components/Content';
import Home from './components/Home';
import dataUser from './components/data/data';
import FormComponent from './components/FormComponent';

function App() {
  // const [data, setData] = useState([])

  // useEffect(() => {
  //   setData([...data, ...dataUser])
  // }, [])

  // const getDataFromChild (obj) => {
  //   console.log(obj)
  // }



  const [data, setData] = useState([
    {
      id: 1,
      title: "KOH KONG KRAV",
      description:
        "Koh Kong Krav Beach is in the 5th place out of 13 beaches in the Koh Kong region The beach is located in a natural place, among the mountains. It is partially covered 	   by trees which give natural shade. It is a spacious coastline with crystal turquoise water and white fine sand, so you don't need special shoes.",
      status: "BEACH",
      peopleGoing: "1537",
    },
    {
      id: 2,
      title: "PHNOM SAMPOV",
      description:
        " This legendary 100 metres high mountain, topped by Wat Sampeou, contains 3 natural caves, lined with Buddhist shrines and statues: Pkasla, Lakhaon and Aksopheak.",
      status: "MOUNTAIN",
      peopleGoing: "81000",
    },
    {
      id: 3,
      title: "KIRIRUM",
      description:
        "Kirirom National Park, a high altitude plateau, is known for its unique high elevation pine forest, which forms the headwaters for numerous streams feeding Kampong 	   	   Speu Town.",
      status: "FOREST",
      peopleGoing: "2500",
    },
  ])

  const getstatefromcard = (value) => {
    const {id,status} = value
    setData([
      ...data.map(e => {
        if(e.id === id) {
          if(e.status === "BEACH"){
            e.status = "MOUNTAIN"
          } else if(e.status === "MOUNTAIN") {
            e.status = "FOREST"
          } else if(e.status === "FOREST") {
            e.status = "BEACH"
          }
        }

        return e
      })
    ])
}

  return (
    <div className='bg-white'>
      <Home data={data} setData={setData} getstatefromcard={getstatefromcard}/>
    </div>
  );
}

export default App;
