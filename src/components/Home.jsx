import React, { Component } from 'react'
import RightSide from '../RightSide'
import Content from './Content'
import LeftSide from './LeftSide'

const Home = ({data,setData, getstatefromcard}) => {

    return (
        <div>
            <div class="grid grid-cols-12 w-full ">
                <div className="bg-newclor">
                    <LeftSide />
                </div>
                <div class=" col-span-8">
                    <Content data={data} setData={setData} getstatefromcard={getstatefromcard}/>
                </div>
                <div id="baner" className=" col-span-3">
                    <RightSide />
                </div>
            </div>
        </div>
    )

}

export default Home

