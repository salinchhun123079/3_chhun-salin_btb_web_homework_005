import React, { useState } from 'react'

function FormComponent({data, setData}) {
    const [newData, setNewData] = useState({})
    const inputHandle = (e) => {
        setNewData({
            ...newData, [e.target.name]: e.target.value
        })
    };

    const submitHandler = (e) => {
        e.preventDefault()
        setData([...data, newData])
    }

    return (
        <div className='w-[500px]  bg-white rounded-xl p-5'>
            <form onSubmit={submitHandler}>
                <label className='text-black font-bold' >Title</label>
                <input type="text" name='title' onChange={inputHandle} className="bg-green-50 border-blue-500 border-2 text-green-900 dark:text-green-400 placeholder-green-700 dark:placeholder-green-500 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5 dark:bg-gray-700 dark:border-green-500 mb-3" placeholder="Sihaknou Ville" />
                <label className='text-black font-bold' >Description</label>
                <input type="text" name='description' onChange={inputHandle} className="bg-green-50 border-blue-500 border-2 text-green-900 dark:text-green-400 placeholder-green-700 dark:placeholder-green-500 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5 dark:bg-gray-700 dark:border-green-500 mb-3" placeholder="Happy place with beautiful beach" />
                <label className='text-black font-bold' >People going</label>
                <input type="text" name='peopleGoing' onChange={inputHandle} className="bg-green-50 border-blue-500 border-2 text-green-900 dark:text-green-400 placeholder-green-700 dark:placeholder-green-500 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5 dark:bg-gray-700 dark:border-green-500 mb-3" placeholder="3200" />
                <label className='text-black font-bold' >Type of adventure</label>
                <select name='status' defaultValue="" onChange={inputHandle} className='border-blue-500 border-2 w-full focus:ring-green-500 focus:border-green-500 rounded-lg'>
                    <option value="" dis>--Please choose an option--</option>
                    <option value="BEACH">Beach</option>
                    <option value="FOREST">Forest</option>
                    <option value="MOUNTAIN">Mountain</option>
                </select>
                <button type='submit' className="btn btn-primary w-25 mt-5">SUBMIT</button>
            </form>
        </div>
    )
}

export default FormComponent