import React, { Component } from 'react'

export class LeftSide extends Component {
  render() {
    return (
      <div>
        <div className='mt-5 w-7 m-auto'><img src="Images/category_icon.png" /></div>
        <div className='mt-16 w-7 m-auto'>
          <div className='mb-9'><img src="Images/cube.png" /></div>
          <div className='mb-9'><img src="Images/list.png" /></div>
          <div className='mb-9'><img src="Images/messenger.png" /></div>
          <div><img src="Images/list.png" /></div>
        </div>
        <div className='mt-16 w-7 m-auto'>
          <div className='mb-9'><img src="Images/success.png" /></div>
          <div className='mb-9'><img src="Images/security.png" /></div>
          <div><img src="Images/users.png" /></div>
        </div>
        <div className='mt-16 w-9  m-auto'>
          <div>
            <img className=" rounded-full mb-9" src="Images/sa.jpg"/>
          </div>
          <div><img className='mb-9 h-9 w-9 rounded-full' src="Images/raamin.jpg" /></div>
          <div><img className='mb-9 h-9 w-9 rounded-full' src="Images/nonamesontheway.jpg" /></div>
          <div><img src="Images/plus.png" className='mb-28' /></div>
        </div>
      </div>
    )
  }
}

export default LeftSide