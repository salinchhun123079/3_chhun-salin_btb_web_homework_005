import React, { useState } from 'react'

import Card from './Card'


function CardList({ data, getstatefromcard }) {
    const [cardState, setCardState] = useState({})
    const [isPopUp, setIsPopUp] = useState(false)
    const getValueFromCard = (props) => {
        setCardState({ ...props })
    }
    return (
        <div className="w-[1225px] grid grid-cols-3 relative">
            {data.map(e => (
                <Card getstatefromcard={getstatefromcard} key={e.id} {...e} getValueFromCard={getValueFromCard} setIsPopUp={setIsPopUp} isPopUp={isPopUp} />
            ))}
            {isPopUp && <div className='absolute w-[500px] min-h-[350px] bg-newclor text-black px-5 pb-5 text-2xl rounded-2xl 
            left-1/2 top-1/2 -translate-x-[30px] -translate-y-[30px] text-justify  
            '>
                <button onClick={() => setIsPopUp(false)}>{<label className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>}</button>
                <h1 className='text-3xl font-bold'>
                    {cardState.title}
                </h1><br />
                <p className='text-gray-750 font-bold text-xl'>
                    {cardState.description}
                </p>
                <br />
                <p className='font-bold text-xl'>Around {cardState.peopleGoing} people going there</p>
                </div>}
        </div>
    )
}

export default CardList