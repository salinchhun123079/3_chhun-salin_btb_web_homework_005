function Card({getValueFromCard, setIsPopUp, isPopUp,getstatefromcard, ...props}) {

   const handleShowPopUp = () => {
    getValueFromCard(props)
    setIsPopUp(!isPopUp)
   }

   const handleChangeStateBtn =()=>{
    getstatefromcard(props)
   }

    return (
        <div key={props.id} className='ml-5'>
            <div className="mb-3 card w-[395px] h-[300px] bg-newclor2 text-white">
                <div className="card-body">
                    <h2 className="card-title">{props.title}</h2>
                    <p className='line-clamp-3'>{props.description}</p>
                    <p>People Going</p>
                    <p className='text-3xl'>{props.peopleGoing}</p>
                    
                    <div className="card-actions flex justify-center justify-around">
                        
                        {/* button trip type */}
                        <button  onClick={handleChangeStateBtn} className={`${props.status === "BEACH" && "bg-blue-500"} ${props.status === "MOUNTAIN" && "bg-gray-500"} ${props.status === "FOREST" && "bg-green-500"} w-36 h-[47.4px] rounded-lg font-bold`}>
                            {/* {props.status === "BEACH" && "BEACH"} {props.status === "MOUNTAIN" && "MOUNTAIN"} {props.status === "FOREST" && "FOREST"} */}
                            {props.status}
                        </button>
                       
                        {/* button show more */}
                        <button onClick={handleShowPopUp} htmlFor="modal1" className="btn w-36 font-bold">READ DETAILS</button>
                
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Card