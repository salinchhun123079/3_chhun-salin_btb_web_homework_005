import React, { Component } from 'react'
import CardList from './Cards/CardList'
import FormComponent from './FormComponent'

const Content = ({ data, setData, getstatefromcard }) => {
    return (
        <div>
            <div className='ml-5 flex items-center mt-16 mb-12 justify-between mr-9'>
                <h1 className='text-4xl text-black font-bold'>Good Evening Team!</h1>
                <label htmlFor="my-modal-3" className="btn">ADD NEW TRIP</label>
                <input type="checkbox" id="my-modal-3" className="modal-toggle" />
                <div className="modal">
                    <div className=" relative">
                        <label htmlFor="my-modal-3" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
                        <FormComponent data={data} setData={setData} />
                    </div>
                </div>
            </div>
            <CardList data={data} setData={setData} getstatefromcard={getstatefromcard} />
        </div>
    )

}

export default Content
